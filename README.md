# echo



## Example Agent Ability
This is an example Ability for an agent, that is compatiable with KADI

## Tagging for releases

* List Tags
`$ git tag`
* Add Tag
Add tag `v0.0.0` to current branch, with message `release v0.0.0`
`$ git tag -a v0.0.0 -m "release v0.0.0"`
* Push Tag to Remote
push all tags to remote
`$ git push origin --tags`
* Delete Tag
removes tag `v0.0.0` from the current branch
`$ git tag -d v0.0.0`
* Remove Tag from Remote
removes the tag listed from origin
` git push origin --delete <tagname>`