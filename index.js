// Base INSTALL ability module for kadi, used to perform initial installation of default kadi abilities
import { fileURLToPath } from 'url';
import path from 'path';
import fs from 'fs';
import chalk from 'chalk';
import { exec } from 'child_process';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

const rootDir           = process.cwd(); 
const abilitiesDir      = path.join(rootDir, 'abilities');
const projectAgentPath     = path.join(rootDir, 'agent.json');

//Gets the agent.json for the ability, so dependencies can be installed
function getAbilityJSON(abilityName, abilityVersion){
    //Get the agent.json for the ability
    return JSON.parse(fs.readFileSync(path.join(abilitiesDir, abilityName, abilityVersion, 'agent.json'), 'utf8'));

}
//Gets the agent.json for the ability, so dependencies can be installed
function getProjectJSON(){
    //Get the agent.json for the project
    return JSON.parse(fs.readFileSync(projectAgentPath, 'utf8'))
}

//Runs a command from ability directory
async function runAbilityCommand(name, version, command) {
    const abilityDir = path.join(rootDir, 'abilities', name, version);
    return new Promise((resolve, reject) => {
        exec(command, { cwd: abilityDir },(error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                reject(error); // Reject the promise on error
                return;
            }
            if (stdout.trim()) {
                console.log(stdout);
            }
            if (stderr.trim()) {
                console.error(stderr);
            }
            resolve(); // Resolve the promise when exec completes successfully
        });
    });
}

//Run a command from project directory root
async function runProjectCommand(command) {
    const abilityDir = path.join(rootDir);
    return new Promise((resolve, reject) => {
        exec(command, { cwd: abilityDir },(error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                reject(error); // Reject the promise on error
                return;
            }
            if (stdout.trim()) {
                console.log(stdout);
            }
            if (stderr.trim()) {
                console.error(stderr);
            }
            resolve(); // Resolve the promise when exec completes successfully
        });
    });
}

//Export for module
const echo = {
    runAbilityCommand: function(name, version, command){
        return runAbilityCommand(name, version, command);
    },
    runProjectCommand: function(command){
        return runProjectCommand(command);
    },
    printProjectJSON: function(){         
        console.log(chalk.white(JSON.stringify(getProjectJSON(), null, 2)));
    },
    printAbilityJSON: function(name, version){
        console.log(chalk.blueBright(JSON.stringify(getAbilityJSON(name, version),null, 2)));
    }
}

export default echo;
